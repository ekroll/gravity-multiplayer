extends Spatial

var index

func disable_collision():
	$wall_mesh/StaticBody/CollisionShape.disabled = true

func enable_collision():
	$wall_mesh/StaticBody/CollisionShape.disabled = false
