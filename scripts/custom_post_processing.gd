extends ViewportContainer

onready var shader = self.get_material() 
onready var enabled = true

func _process(delta):
	pass

func toggle():
	enabled = !enabled
	shader.set_shader_param("enable", enabled)
	print("toggled custom post processing")

func enable():
	enabled = true
	shader.set_shader_param("enable", enabled)
	print("enabled custom post processing")

func disable():
	enabled = false
	shader.set_shader_param("enable", enabled)
	print("disabled custom post processing")
