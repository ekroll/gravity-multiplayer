extends Spatial

var amount_of_islands = 1
var max_exent_radius = 0
var min_exent_radius = 0
var max_spawn_height = 0
var min_spawn_height = 0


onready var global = get_node("/root/global")
var floating_island = load("res://scenes/3D/gravity_world/generator/floating_island.tscn")
var rng = RandomNumberGenerator.new()

func _ready():
	self.translation = Vector3(0, 0, 0)

func generate(island_generator_seed):
	print("generating floating islands")
	print("island seed: ", island_generator_seed)
	print("amount of islands ", amount_of_islands)
	rng.set_seed(island_generator_seed)
	
	var counter = 0
	for island in range(0, amount_of_islands):
		counter += 1
		var new_floating_island = floating_island.instance() 
		var random_island_translation_2D = global.rand_point_between_radiuses(rng, min_exent_radius, max_exent_radius)
			
		new_floating_island.translation.x = random_island_translation_2D.x
		new_floating_island.translation.z = random_island_translation_2D.y
		new_floating_island.translation.y = rng.randi_range(min_spawn_height, max_spawn_height)
		
		#if counter % 2 == 0:
			#floating_island.rotation.x = 180
		#else:
			#floating_island.rotation.x = 0
			
		
		self.add_child(new_floating_island)
		print("added floating island")
	return
