extends KinematicBody
var inactive = true

var players
var closest_player
var distance_to_closest_player = 1000000000000
var target

var can_fire_distance = 30
var closest_distance_to_players = 5.0

var movement_direction = Vector3()
var UP = Vector3(0, 1, 0)
onready var global = get_node("/root/global")


func _process(delta):
	if (!inactive):
		if (global.is_in_game):
			players = get_viewport().get_camera().get_owner().get_parent()
			
			for player in players.get_children():
				var distance_to_player = self.global_transform.origin.distance_to(player.translation)
				
				if (distance_to_player < distance_to_closest_player):
					distance_to_closest_player = distance_to_player
					closest_player = player
			
			if (closest_player.translation.y + 1.85 > 0):
				target = UP
				self.translation -= Vector3(0, 0, 0)
			else:
				target = closest_player.translation
				look_at(target, Vector3(0, 1, 0))
				self.translation -= (self.translation - closest_player.translation).normalized() * delta * 5
	if self.translation.y > 0:
		self.translation.y *= -1.0
			
		
func _on_body_enter_death_ray(unlucky_thing):
	if !inactive:
		if (unlucky_thing.has_method("die")):
				global.respawn(unlucky_thing, unlucky_thing.team)
