# About the Game
Gravity is a multiplayer FPS prototype where you can make gravity go up or down as you wish.
The world consists of floating islands and stuff at random positions per new game, and a simple underwater world.


# About the Project
Gravity Multiplayer is a result of my interest in making games and programming. The project is made with Godot 3.2.1 and div. other software.
Im posting my source code under the [GPLv3](https://opensource.org/licenses/gpl-3.0.html) in case someone finds it useful.


# Game Releases
Game releases can be found [here](https://ekroll.itch.io/gravity-multiplayer) (built with gitlab-ci from release branch)