extends Spatial

var amount_of_clouds = 200
var exents = Vector3(200, 100, 200)
var cloud_scale = 0.01

onready var global = get_node("/root/global")
var cloud = load("res://scenes/3D/gravity_world/generator/cloud.tscn")
var rng = RandomNumberGenerator.new()

func _ready():
	#self.translation.x = -exents.x / 2
	#self.translation.z = -exents.z / 2
	pass

func generate(cloud_generator_seed):
	print("generating clouds")
	print("    verify cloud seed: ", cloud_generator_seed)
	rng.set_seed(cloud_generator_seed)
	if cloud_generator_seed != null:
		for every_cloud in range(0, amount_of_clouds):
			var new_cloud = cloud.instance()
			var random_cloud_translation = Vector3(rng.randi_range(1, exents.x), rng.randi_range(1, exents.y), rng.randi_range(1, exents.z))
			new_cloud.translation = random_cloud_translation
			new_cloud.scale = new_cloud.scale * cloud_scale
			self.add_child(new_cloud)
	return
