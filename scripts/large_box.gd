extends Spatial

var index

func disable_collision():
	$large_box/StaticBody/CollisionShape.disabled = true

func enable_collision():
	$large_box/StaticBody/CollisionShape.disabled = false
