extends ToolButton

var toggle_state
var name_with_slashes
#####################################################
onready var name_button = $GridContainer/NameButton

func _ready():
	if self.name.find(" -> ") > -1:
		name_with_slashes = self.name.replace(" -> ", "/")
		
		var new_name = self.name.split(" -> ")
		var index = new_name.size() - 1
		name_button.text = new_name[index]
	
	else:
		name_button.text = self.name
	
	self.text = str("Toggle ", name_button.text)

func on_property_toggled(button_pressed):
	toggle_state = button_pressed
	self.emit_signal("property_toggled", name_with_slashes, toggle_state)

signal property_toggled(property_name, toggle_state)


