extends Control

var toolbar_enabled = true
var move_window = false
var drag_start

func _on_toolbar_input(event):
	if event is InputEventMouseButton:
		if toolbar_enabled:
			move_window = !move_window
			drag_start = get_local_mouse_position()

func _process(delta):
	if move_window:
		OS.window_position = OS.window_position + get_global_mouse_position() - drag_start
		
func _on_close_window():
	get_tree().quit()

func show():
	self.visible = true

func hide():
	self.visible = false
