extends Spatial

var amount_of_seagull_clusters = 25
var min_spawn_height = 25
var exents = Vector3(100, 50, 100)
var seagull_scale = 1

onready var global = get_node("/root/global")
var seagulls = load("res://scenes/3D/NPC/seagull_cluster.tscn")
var rng = RandomNumberGenerator.new()

func _ready():
	self.translation.x = -exents.x / 2
	self.translation.z = -exents.z / 2

func generate(seagull_generator_seed):
	print("generating seagulls")
	print("    verify seagull seed: ", seagull_generator_seed)
	rng.set_seed(seagull_generator_seed)
	if seagull_generator_seed != null:
		for every_seagull in range(0, amount_of_seagull_clusters):
			var new_seagulls = seagulls.instance()
			var random_seagulls_translation = Vector3(rng.randi_range(1, exents.x), rng.randi_range(min_spawn_height, exents.y), rng.randi_range(1, exents.z))
			new_seagulls.translation = random_seagulls_translation
			new_seagulls.scale = new_seagulls.scale * seagull_scale
			self.add_child(new_seagulls)
	return
