extends Camera

export var mouse_sensitivity = Vector2(0.001, 0.001)
var pitch = 0

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event):
	#Code for looking around with camera using a mouse
	if event is InputEventMouseMotion:
		pitch = clamp(pitch - event.relative.y * mouse_sensitivity.y, -PI/2, PI/2)
		self.rotation.x = pitch
		self.rotation.y -= event.relative.x * mouse_sensitivity.x
