extends Spatial

var index

func disable_collision():
	$staticbody/slope.disabled = true
	$staticbody/top_step.disabled = true
	$staticbody/top_step2.disabled = true
	$staticbody/side.disabled = true
	$staticbody/side2.disabled = true
	$staticbody/triangle.disabled = true
	$staticbody/triangle2.disabled = true

func enable_collision():
	$staticbody/slope.disabled = false
	$staticbody/top_step.disabled = false
	$staticbody/top_step2.disabled = false
	$staticbody/side.disabled = false
	$staticbody/side2.disabled = false
	$staticbody/triangle.disabled = false
	$staticbody/triangle2.disabled = false
