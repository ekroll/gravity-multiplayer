extends Spatial

var amount_of_fish_clusters = 100
var exents = Vector3(100, -50, 100)
var fish_scale = 10

onready var global = get_node("/root/global")
var fishes = load("res://scenes/3D/NPC/large_fish_cluster.tscn")
var rng = RandomNumberGenerator.new()

func _ready():
	self.translation.x = -exents.x / 2
	self.translation.z = -exents.z / 2

func generate(fish_generator_seed):
	print("generating fish")
	print("verify fish seed: ", fish_generator_seed)
	rng.set_seed(fish_generator_seed)

	for fish in range(0, amount_of_fish_clusters):
		print("added fish cluster")
		var new_fishes = fishes.instance()
		var random_fishes_translation = Vector3(rng.randi_range(1, exents.x), rng.randi_range(exents.y + 7, -15), rng.randi_range(1, exents.z))
		new_fishes.translation = random_fishes_translation
		print(random_fishes_translation)
		new_fishes.scale = new_fishes.scale * fish_scale
		self.add_child(new_fishes)
	return
