extends Spatial

var pool_side_settings = {"length_from_center": 50,
													"pool_depth": 50,
													"pool_width": 100,
													"center_floor_radius": 0
												 }

onready var walk_thing = $walk_thing
onready var wall = $wall

func make_pool_side():
	make_walk_thing()
	make_wall()

func make_walk_thing():
	walk_thing.mesh.size.x = pool_side_settings["length_from_center"] - pool_side_settings["center_floor_radius"]
	walk_thing.translation.x = -1 * (pool_side_settings["length_from_center"] / 2) - pool_side_settings["center_floor_radius"]
	walk_thing.create_trimesh_collision()


func make_wall():
	wall.translation.x = pool_side_settings["length_from_center"] + 0.75
	wall.translation.y = -1 * (pool_side_settings["pool_depth"] / 2) + 1
	wall.mesh.size.y = pool_side_settings["pool_depth"]
	wall.mesh.size.z = pool_side_settings["pool_width"] + 4.5
	wall.create_trimesh_collision()
