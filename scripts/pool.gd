extends Spatial

var settings = {"water_volume": Vector3(100, 50, 100),
										 "center_floor_radius": 5
										}

onready var pool_side_negative_x = $pool_side
onready var pool_side_positive_x = $pool_side3
onready var pool_side_negative_z = $pool_side4
onready var pool_side_positive_z = $pool_side2
onready var pool_floor = $pool_floor

func setup():
	pool_side_negative_x.pool_side_settings["length_from_center"] = settings["water_volume"].x / 2
	pool_side_negative_x.pool_side_settings["pool_depth"] = settings["water_volume"].y
	pool_side_negative_x.pool_side_settings["pool_width"] = settings["water_volume"].z
	pool_side_negative_x.pool_side_settings["center_floor_radius"] = settings["center_floor_radius"]
	pool_side_negative_x.make_pool_side()

	pool_side_positive_x.pool_side_settings["length_from_center"] = settings["water_volume"].x / 2
	pool_side_positive_x.pool_side_settings["pool_depth"] = settings["water_volume"].y
	pool_side_positive_x.pool_side_settings["pool_width"] = settings["water_volume"].z
	pool_side_positive_x.pool_side_settings["center_floor_radius"] = settings["center_floor_radius"]
	pool_side_positive_x.make_pool_side()

	pool_side_negative_z.pool_side_settings["length_from_center"] = settings["water_volume"].z / 2
	pool_side_negative_z.pool_side_settings["pool_depth"] = settings["water_volume"].y
	pool_side_negative_z.pool_side_settings["pool_width"] = settings["water_volume"].x
	pool_side_negative_z.pool_side_settings["center_floor_radius"] = settings["center_floor_radius"]
	pool_side_negative_z.make_pool_side()

	pool_side_positive_z.pool_side_settings["length_from_center"] = settings["water_volume"].z / 2
	pool_side_positive_z.pool_side_settings["pool_depth"] = settings["water_volume"].y
	pool_side_positive_z.pool_side_settings["pool_width"] = settings["water_volume"].x
	pool_side_positive_z.pool_side_settings["center_floor_radius"] = settings["center_floor_radius"]
	pool_side_positive_z.make_pool_side()

	pool_floor.translation.y = -settings["water_volume"].y
	pool_floor.mesh.size.x = settings["water_volume"].x
	pool_floor.mesh.size.z = settings["water_volume"].z
	pool_floor.create_trimesh_collision()
	return
