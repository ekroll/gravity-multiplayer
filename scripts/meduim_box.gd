extends Spatial

var index

func disable_collision():
	$box_mesh/StaticBody/CollisionShape.disabled = true

func enable_collision():
	$box_mesh/StaticBody/CollisionShape.disabled = false