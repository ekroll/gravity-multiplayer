extends Node

export var world_exents = Vector3(200, 500, 200)
export var sea_depth = 50

var max_world_radius
var world_seed

func _ready():
	max_world_radius
	if world_exents.x > world_exents.z:
		max_world_radius = world_exents.z / 2
	else:
		max_world_radius = world_exents.x / 2

export var cloud_settings = {
														 "amount_of_clouds": 350,
														 "min_spawn_height": 500,
														 "cloud_scale": 4
														}

export var seagull_settings = {
															 "amount_of_seagull_clusters": 10,
															 "min_spawn_height": 20,
															 "max_spawn_height": 50,
															 "seagull_scale": 1
															}

export var floating_island_settings = {
																			 "amount_of_islands": 10,
																			 "min_spawn_height": 20,
																			 "min_radius": 15,
																			}

export var powerup_hut_settings = {
																	 "amount_of_huts": 5,
																	 "min_spawn_height": 5,
																	 "min_radius": 10,
																	}

export var rock_settings = {
														"amount_of_rocks": 50,
														"rock_scale": 3.5
														}

export var fish_settings = {
														"amount_of_fish_clusters": 50,
														"fish_scale": 1
													 }

#parent nodes
onready var clouds = get_node("clouds")
onready var seagulls = get_node("seagulls")
onready var floating_islands = get_node("floating_islands")
onready var powerup_huts = get_node("powerup_huts")
onready var sea = get_node("sea")
onready var floating_rocks = get_node("floating_rocks")
onready var fishes = $fishes
onready var pool = get_node("pool")
onready var barrier = $barrier

#world generation
remote func generate_world(new_seed):
	world_seed = new_seed

	print("making seeds from world seed")
	var cloud_seed = int(world_seed * 1.5)
	var island_seed = int(world_seed * 2)
	var hut_seed = int(world_seed / 3)
	var rock_seed = int(world_seed / 5)
	var fish_seed = int(world_seed * 3)

	update_clouds()
	update_seagulls()
	update_floating_islands()
	#update_powerup_huts()
	update_rocks()
	update_fishes()
	update_sea()
	update_pool()
	update_barrier()

	pool.setup()

	var generator_thread = Thread.new()
	generator_thread.start(barrier, "setup", null, 2)
	generator_thread.wait_to_finish()

	var generator_thread1 = Thread.new()
	generator_thread1.start(sea, "setup", null, 2)
	generator_thread1.wait_to_finish()

	var generator_thread2 = Thread.new()
	generator_thread2.start(clouds, "generate", cloud_seed, 2)
	generator_thread2.wait_to_finish()

	var generator_thread3 = Thread.new()
	generator_thread3.start(fishes, "generate", fish_seed, 2)
	generator_thread3.wait_to_finish()

	var generator_thread4 = Thread.new()
	generator_thread4.start(seagulls, "generate", fish_seed, 2)
	generator_thread4.wait_to_finish()

	var generator_thread5 = Thread.new()
	generator_thread5.start(floating_islands, "generate", island_seed, 1)
	generator_thread5.wait_to_finish()

	var generator_thread6 = Thread.new()
	generator_thread6.start(floating_rocks, "generate", rock_seed, 1)
	generator_thread6.wait_to_finish()

	#var generator_thread7 = Thread.new()
	#generator_thread7.start(powerup_huts, "generate", hut_seed, 0)
	#generator_thread7.wait_to_finish()

#functions to update individual variables
func update_clouds():
	if clouds.get_children().size() > 0:
		for cloud in clouds.get_children():
			cloud.queue_free()
	var cloud_exents = world_exents + Vector3(10000, 0, 10000)
	clouds.amount_of_clouds = cloud_settings["amount_of_clouds"]
	clouds.cloud_scale = cloud_settings["cloud_scale"]
	clouds.exents = cloud_exents
	clouds.translation.y = cloud_settings["min_spawn_height"]
	print("done updating clouds")

func update_seagulls():
	if seagulls.get_children().size() > 0:
		for seagull in seagulls.get_children():
			seagull.queue_free()
	seagulls.amount_of_seagull_clusters = seagull_settings["amount_of_seagull_clusters"]
	seagulls.exents.x = world_exents.x
	seagulls.exents.z = world_exents.z
	seagulls.exents.y = seagull_settings["max_spawn_height"]
	seagulls.min_spawn_height = seagull_settings["min_spawn_height"]
	seagulls.seagull_scale = seagull_settings["seagull_scale"]
	seagulls.min_spawn_height = seagull_settings["min_spawn_height"]
	seagulls.translation.y = seagull_settings["min_spawn_height"]
	print("done updating seagulls")


func update_floating_islands():
	if floating_islands.get_children().size() > 0:
		for floating_island in floating_islands.get_children():
			floating_island.queue_free()
	floating_islands.amount_of_islands = floating_island_settings["amount_of_islands"]
	floating_islands.max_exent_radius = max_world_radius - 20
	floating_islands.min_exent_radius = floating_island_settings["min_radius"]
	floating_islands.max_spawn_height = cloud_settings["min_spawn_height"] / 2 - 15
	floating_islands.min_spawn_height = floating_island_settings["min_spawn_height"]
	print("done updating islands")

func update_powerup_huts():
	if powerup_huts.get_children().size() > 0:
		for powerup_hut in powerup_huts.get_children():
			powerup_hut.queue_free()
	powerup_huts.amount_of_huts = powerup_hut_settings["amount_of_huts"]
	powerup_huts.max_exent_radius = max_world_radius - 20
	powerup_huts.min_exent_radius = powerup_hut_settings["min_radius"]
	powerup_huts.max_spawn_height = floating_island_settings["min_spawn_height"]
	powerup_huts.min_spawn_height = powerup_hut_settings["min_spawn_height"]
	print("done updating huts")

func update_rocks():
	if floating_rocks.get_children().size() > 0:
		for floating_rock in floating_rocks.get_children():
			floating_rock.queue_free()
	floating_rocks.amount_of_rocks = rock_settings["amount_of_rocks"]
	floating_rocks.rock_scale = rock_settings["rock_scale"]
	floating_rocks.exents.x = world_exents.x
	floating_rocks.exents.y = -sea_depth
	floating_rocks.exents.z = world_exents.z
	print("done updating rocks")

func update_fishes():
	if fishes.get_children().size() > 0:
		for fish in fishes.get_children():
			fish.queue_free()
	fishes.amount_of_fish_clusters = fish_settings["amount_of_fish_clusters"]
	fishes.fish_scale = fish_settings["fish_scale"]
	fishes.exents.x = world_exents.x
	fishes.exents.z = -sea_depth
	fishes.exents.z = world_exents.z
	print("done updating fish")

func update_sea():
	sea.settings["size_meters"].x = world_exents.x
	sea.settings["size_meters"].y = world_exents.z
	sea.settings["subdivisions_per_meter"] = 2
	print("done updating sea")

func update_pool():
	var pool_settings = pool.settings
	pool.settings["water_volume"].x = world_exents.x
	pool.settings["water_volume"].y = sea_depth
	pool.settings["water_volume"].z = world_exents.z
	pool.settings["center_floor_radius"] = 0
	print("done updating pool")

func update_barrier():
	barrier.map_size.x = world_exents.x
	barrier.map_size.y = world_exents.z
	barrier.barrier_height = world_exents.y

func get_random_seed():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var random_seed = rng.randi()
	print("new random seed = ", random_seed)
	return random_seed
