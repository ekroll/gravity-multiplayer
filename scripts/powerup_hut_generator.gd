extends Spatial
var amount_of_huts = 20
var max_exent_radius = 50
var min_exent_radius = 50
var max_spawn_height = 100
var min_spawn_height = -100

onready var global = get_node("/root/global")
var flying_hut = load("res://scenes/3D/gravity_world/generator/powerup_hut.tscn")
var rng = RandomNumberGenerator.new()

func _ready():
	self.translation = Vector3(0, 0, 0)
	
func generate(hut_generator_seed):
	print("generating powerup_huts")
	print("verify hut seed: ", hut_generator_seed)
	rng.set_seed(hut_generator_seed)
	
	for every_island in range(0, amount_of_huts):
		
		var new_flying_hut = flying_hut.instance() 
		var random_island_translation_2D = global.rand_vec2_in_radius(rng, max_exent_radius, max_exent_radius, false)
				
		new_flying_hut.translation.x = random_island_translation_2D.x
		new_flying_hut.translation.z = random_island_translation_2D.y
		new_flying_hut.translation.y = rng.randi_range(min_spawn_height, max_spawn_height)
		self.add_child(new_flying_hut)
		print("spawned powerup_hut")
	return
