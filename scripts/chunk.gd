extends Spatial
class_name Chunk

var x
var z
var chunk_size
var chunk_detail_multiplier
var noise

var remove

func _init(x, z, chunk_size, chunk_detail_multiplier, noise):
	self.x = x
	self.z = z
	self.chunk_size = chunk_size
	self.chunk_detail_multiplier = chunk_detail_multiplier
	self.noise = noise

func generate_chunk():
	var plane = PlaneMesh.new()
	plane.size = Vector2(chunk_size, chunk_size)
	plane.subdivide_width = chunk_detail_multiplier
	plane.subdivide_depth = chunk_detail_multiplier
	
	var surface = SurfaceTool.new()
	surface.create_from(plane, 0)
	var mesh_data = MeshDataTool.new()
	var mesh_array = surface.commit()
	var error = mesh_data.create_from_surface(mesh_array, 0)
	
	for index in range(mesh_data.get_vertex_count()):
		var vertex = mesh_data.get_vertex(index)
		vertex.y = noise.get_noise_3d(vertex.x + x, vertex.y, vertex.z + z)
		mesh_data.set_vertex(index, vertex)
		
	for surface in mesh_array.get_surface_count():
		mesh_array.surface_remove(surface)
	
	mesh_data.commit_to_surface(mesh_array)
	surface.begin(Mesh.PRIMITIVE_TRIANGLES)
	surface.create_from(mesh_array, 0)
	surface.generate_normals()
	
	var mesh_instance = MeshInstance.new()
	mesh_instance.mesh = surface.commit()

func _ready():
	generate_chunk()
