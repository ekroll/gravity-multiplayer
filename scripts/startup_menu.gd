extends Node2D

onready var server = get_node("/root/server")
onready var client = get_node("/root/client")

func _on_choose_server(): #A function that is called if you choose "Play" at the startup menu
	server.visible = true
	self.queue_free()

func _on_choose_client(): #Pressed the client button at main menu
	client.visible = true
	self.queue_free()
