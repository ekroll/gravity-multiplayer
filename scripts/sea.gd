extends Spatial

var settings = {
								"size_meters": Vector2(100, 100),
								"subdivisions_per_meter": 0.25
							 }

func printstuff():
	print(settings["size_meters"])

func setup(nothing):
	$sea_mesh.mesh.size.x = settings["size_meters"].x
	$sea_mesh.mesh.size.y = settings["size_meters"].y
	print("set sea size to ", settings["size_meters"])

	#subdivide mesh
	if ($sea_mesh.mesh.subdivide_width != (settings["size_meters"].x - 1) * settings["subdivisions_per_meter"]):
		$sea_mesh.mesh.subdivide_width = (settings["size_meters"].x - 1) * settings["subdivisions_per_meter"]
	if ($sea_mesh.mesh.subdivide_depth != (settings["size_meters"].y - 1) * settings["subdivisions_per_meter"]):
		$sea_mesh.mesh.subdivide_depth = (settings["size_meters"].y - 1) * settings["subdivisions_per_meter"]
	print("subdivided sea mesh")

	#set shader params
	print("setting shader params")
	var sea_shader = $sea_mesh.get_surface_material(0)
	sea_shader.set_shader_param("sea_size_meters", settings["size_meters"])

	#making small_noise
	var small_noise_texture = NoiseTexture.new()
	small_noise_texture.set_width(2048)
	small_noise_texture.set_height(2048)
	small_noise_texture.noise = OpenSimplexNoise.new()
	small_noise_texture.seamless = true
	sea_shader.set_shader_param("small_noise", small_noise_texture)
	#making small_normalmap
	var small_noise_normalmap = NoiseTexture.new()
	small_noise_normalmap.set_width(2048)
	small_noise_normalmap.set_height(2048)
	small_noise_normalmap.as_normalmap = true
	small_noise_normalmap.seamless = true
	small_noise_normalmap.bump_strength = 2
	small_noise_normalmap.noise = OpenSimplexNoise.new()
	sea_shader.set_shader_param("small_normalmap", small_noise_normalmap)

	#making medium_noise
	var medium_noise_texture = NoiseTexture.new()
	medium_noise_texture.set_width(2048)
	medium_noise_texture.set_height(2048)
	medium_noise_texture.noise = OpenSimplexNoise.new()
	medium_noise_texture.seamless = true
	sea_shader.set_shader_param("medium_noise", medium_noise_texture)
	#making medium_normalmap
	var medium_noise_normalmap = NoiseTexture.new()
	medium_noise_normalmap.set_width(2048)
	medium_noise_normalmap.set_height(2048)
	medium_noise_normalmap.as_normalmap = true
	medium_noise_normalmap.seamless = true
	medium_noise_normalmap.bump_strength = 4
	medium_noise_normalmap.noise = OpenSimplexNoise.new()
	sea_shader.set_shader_param("medium_normalmap", medium_noise_normalmap)


	#making large_noise
	var large_noise_texture = NoiseTexture.new()
	large_noise_texture.set_width(2048)
	large_noise_texture.set_height(2048)
	large_noise_texture.seamless = true
	large_noise_texture.noise = OpenSimplexNoise.new()
	sea_shader.set_shader_param("large_noise", large_noise_texture)
	#making large_normalmap
	var large_noise_normalmap = NoiseTexture.new()
	large_noise_normalmap.set_width(2048)
	large_noise_normalmap.set_height(2048)
	large_noise_normalmap.as_normalmap = true
	large_noise_normalmap.seamless = true
	large_noise_normalmap.bump_strength = 6
	large_noise_normalmap.noise = OpenSimplexNoise.new()
	sea_shader.set_shader_param("large_normalmap", large_noise_normalmap)
	return
