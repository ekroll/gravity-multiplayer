extends Node2D

onready var viewport = $viewport_container/viewport
onready var custom_post_processing = $viewport_container

var low_graphics = false

#Global value holders
var world 
var screen_resolution
var screen_dpi
var monitor_width
var fov

func _ready():
	print(" ")
	print("------------startup------------")
	print("application started with arguments:") #my currently failing attempt on headless-server stuff
	var arguments = {}
	for argument in OS.get_cmdline_args():
		if argument.find("=") > -1:
			var key_value = argument.split("=")
			arguments[key_value[1].lstrip("--")] = key_value[1]

	if (arguments.size() > 0):
		for argument in arguments:
			print(argument)
			if argument == "host":
				#do_something_cool
				pass
	else:
		print("none")
	print(" ")
	
	print("looking for vr interfaces... (first one will be used)")
	if (ARVRServer.get_interfaces().size() > 0):
		print("found vr interfaces: ")
		for interface in ARVRServer.get_interfaces():
			print(interface)
	else:
		print("no interfaces to be found")
	print(" ")
	
	screen_resolution = OS.get_screen_size()
	screen_dpi = OS.get_screen_dpi()
	monitor_width = screen_resolution.x / screen_dpi
	
	custom_post_processing.rect_size = screen_resolution
	viewport.size = screen_resolution
	fov = monitor_width * 5
	
	world = viewport.get_child(0)




func toggle_low_graphics():
	low_graphics = !low_graphics
	global.game.world.sun.visible = !low_graphics
	custom_post_processing.toggle()
	OS.vsync_enabled = low_graphics

	if low_graphics:
		viewport.set_msaa(Viewport.MSAA_DISABLED)
		print("disabled MSAA")
		viewport.set_usage(Viewport.USAGE_3D_NO_EFFECTS)
	else:
		viewport.set_msaa(Viewport.MSAA_8X)
		print("enabled MSAA")
		viewport.set_usage(Viewport.USAGE_3D)


