shader_type spatial;
render_mode cull_back, unshaded;
varying vec3 original_vertex_position;
varying vec3 normal;

vec3 calculate_normal_the_wrong_way(vec3 original_vertex, vec3 current_vertex) {
	vec3 new_normal = vec3(0, 1, 0);
	new_normal += current_vertex - original_vertex;
	new_normal.y = abs(new_normal.y + 1.0);
	new_normal = normalize(new_normal);
	return new_normal.xyz;
}

void vertex() {
	original_vertex_position = VERTEX;
	VERTEX.y += 0.75 * sin(-VERTEX.x / 1.0 + TIME * 1.0);
	VERTEX.x += 0.75 * cos(VERTEX.z / 1.0 + TIME * 1.0);
	VERTEX.z += 0.75 * cos(-VERTEX.z / 1.0 + TIME * 1.0);
	
	normal = calculate_normal_the_wrong_way(VERTEX, original_vertex_position);
	NORMAL = normal;
}

void fragment() {
	vec2 refraction_distortion_uv = SCREEN_UV + (normal.xz * 1.0) * SCREEN_UV / 4.0;
	vec3 refraction_texture = textureLod(SCREEN_TEXTURE, refraction_distortion_uv, 1.5).rgb;
	
	//ALBEDO = vec3(1.0, 1.0, 2.0);
	ALBEDO = refraction_texture;
}