extends Node2D
var default_port = 50000
var default_max_amount_of_players = 10

onready var global = get_node("/root/global")
onready var game
onready var server_ip_label = $ip_label
onready var server_port_text_edit = $port_text_edit
onready var server_max_players_text_edit = $max_players_text_edit




func _ready():
	print("ready server.gd")
	self.hide()
	server_ip_label.text = str("Your local IP: ", IP.get_local_addresses()[1])
	server_port_text_edit.text = str(default_port)
	server_max_players_text_edit.text = str(default_max_amount_of_players)
	get_tree().connect("network_peer_connected", self, "player_connected_to_server")
	get_tree().connect("network_peer_disconnected", self, "peer_disconnected")




func _on_start_server(): #called after pressing start as server
	self.visible = false
	start_server()




func start_server():
	make_server(int(server_port_text_edit.text), int(server_max_players_text_edit.text))
	OS.set_window_fullscreen(true)
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	global.create_game()
	global.game.world.generator.generate_world(global.game.world.generator.get_random_seed())
	global.game.world.add_new_player(1)
	global.music.fade_out(global.music.current_stream_player)
	if (global.vr):
		global.check_vr()




func make_server(port, max_players): #A function that easily starts a server. will not run unless you call it
	global.peer = NetworkedMultiplayerENet.new()
	global.peer.create_server(port, max_players)
	print("created server at ip ", IP.get_local_addresses()[1], ", and port ", String(port))
	print("the max amount of players is ", String(max_players))
	print(" ")
	get_tree().set_network_peer(global.peer)
	return global.peer




func player_connected_to_server(peer_id):
	print("player ", peer_id, " is here")

func player_disconnected(peer_id):
	rpc("kick_player", peer_id, "kicked for disconnecting")




#These new rpc_ts() functions means RemoteProcedureCall_ThroughServer(). Made to never expose a clients IP.
remote func rpc_ts(function, arguments_array):
	var id = get_tree().get_rpc_sender_id()
	for player_name in global.game.world.get_player_names():
		if int(player_name != id):
			rpc_id(int(player_name), str(function), arguments_array)

remote func rpc_id_ts(id, function, arguments_array):
	rpc_id(int(id), str(function), arguments_array)




remote func rset_ts(variable_name, new_value):
	var id = get_tree().get_rpc_sender_id()
	for player_name in global.game.world.get_player_names():
		if int(player_name != id):
			rset_id(int(player_name), str(variable_name), new_value)




sync func kick_player(id, reason): # A function to kick a player if needed.
	if get_tree().is_network_server():
		for player_name in global.game.world.get_player_names():
			if int(player_name) == id:
				global.game.world.players.get_node(str(player_name)).queue_free()
		print("player ", id, " was kicked because of ", str(reason))
