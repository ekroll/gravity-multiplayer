extends Spatial

var can_shoot = true
var fire_rate = 0.075
var shooter_id
onready var root = get_node("/root")
onready var global = get_node("/root/global")
onready var paintball = load("res://scenes/3D/player_character/paintball.tscn")
onready var ball_exit = $gun_mesh/ball_exit
onready var blaster_sound = $gun_mesh/ball_exit/blaster_sound
onready var raycast = $gun_mesh/ball_exit/RayCast

func _ready():
	shooter_id = get_owner().name

sync func interact(shooter, shooter_id):
	if can_shoot:
		shoot(shooter, shooter_id)
		can_shoot = false
		global.call_delayed(self, "can_shoot", fire_rate)

func shoot(shooter, shooter_id):
	blaster_sound.play()
	var new_paintball = paintball.instance()
	new_paintball.transform = ball_exit.global_transform
	new_paintball.shooter_id = shooter_id
	new_paintball.set_network_master(shooter_id)
	global.game.world.paintballs.add_child(new_paintball)

func can_shoot():
	can_shoot = true
