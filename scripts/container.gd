extends Spatial

var index

func disable_collision():
	$container_roof/StaticBody/CollisionShape.disabled = true
	$container_wall_1/StaticBody/CollisionShape.disabled = true
	$container_wall_2/StaticBody/CollisionShape.disabled = true
	$container_floor/StaticBody/CollisionShape.disabled = true

func enable_collision():
	$container_roof/StaticBody/CollisionShape.disabled = false
	$container_wall_1/StaticBody/CollisionShape.disabled = false
	$container_wall_2/StaticBody/CollisionShape.disabled = false
	$container_floor/StaticBody/CollisionShape.disabled = false
