extends Node

var current_stream_player
var fade_transition_seconds = 2

func _ready():
	print("ready music.gd")
	play("main_theme")
	
func play(stream_name):
	if self.get_children().size() > 0:
		for stream_player in self.get_children():
			if stream_player.name == stream_name:
				current_stream_player = stream_player
				current_stream_player.play()

func fade_out(stream_player):
	var stop_tween = Tween.new()
	stop_tween.connect("tween_all_completed", self, "stop")
	stop_tween.interpolate_property(stream_player, "volume_db", 0, -80, fade_transition_seconds, 0, Tween.EASE_IN, 0)
	stop_tween.start()
	add_child(stop_tween)
	print("fading out ", stream_player)

func stop():
	current_stream_player.stop()