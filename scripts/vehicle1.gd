extends VehicleBody

############################################################
# behaviour values
var rest
onready var body_collision = $vehicle_body_collission

export var MAX_ENGINE_FORCE = 400.0
export var MAX_BRAKE_FORCE = 5.0
export var MAX_STEER_ANGLE = 0.6

export var steer_speed = 7.0

var steer_target = 0.0
var steer_angle = 0.0

############################################################
# Input

export var joy_steering = JOY_ANALOG_LX
export var steering_mult = -1.0
export var joy_throttle = JOY_ANALOG_R2
export var throttle_mult = 1.0
export var joy_brake = JOY_ANALOG_L2
export var brake_mult = 1.0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _physics_process(delta):
	if !rest:
		body_collision.disabled = false
		self.visible = true
		self.MODE_RIGID
		var steer_val = steering_mult * Input.get_joy_axis(0, joy_steering)
		var throttle_val = throttle_mult * Input.get_joy_axis(0, joy_throttle)
		var brake_val = brake_mult * Input.get_joy_axis(0, joy_brake)
	
		# overrules for keyboard
		if Input.is_action_pressed("forward"):
			throttle_val = 1.0
		if Input.is_action_pressed("backwards"):
			brake_val = 1.0
		if Input.is_action_pressed("eft"):
			steer_val = 1.0
		elif Input.is_action_pressed("right"):
			steer_val = -1.0
		
		engine_force = throttle_val * MAX_ENGINE_FORCE
		brake = brake_val * MAX_BRAKE_FORCE
		
		steer_target = steer_val * MAX_STEER_ANGLE
		if (steer_target < steer_angle):
			steer_angle -= steer_speed * delta
			if (steer_target > steer_angle):
				steer_angle = steer_target
		elif (steer_target > steer_angle):
			steer_angle += steer_speed * delta
			if (steer_target < steer_angle):
				steer_angle = steer_target
	
		steering = steer_angle
	else:
		body_collision.disabled = true
		self.MODE_KINEMATIC
		self.visible = false
	
		
