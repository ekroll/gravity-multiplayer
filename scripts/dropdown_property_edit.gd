extends ToolButton

var options = ["something", "doesnt", "work"] #overwritten by something (depends how its used)
var property_value
var name_with_slashes
#######################################################
onready var name_button = $GridContainer/NameButton
onready var option_button = $GridContainer/OptionButton

func _ready():
	if self.name.find(" -> ") > -1:
		name_with_slashes = self.name.replace(" -> ", "/")
		
		var new_name = self.name.split(" -> ")
		var index = new_name.size() - 1
		name_button.text = new_name[index]
	
	else:
		name_button.text = self.name
	
	self.text = str("Set ", name_button.text)
	
	for option in options:
		option_button.add_item(str(option))

func on_property_set(ID):
	property_value = option_button.get_item_text(ID)
	self.emit_signal("property_set", name_with_slashes, property_value)
	
signal property_set(property_name, property_value)
	
	
