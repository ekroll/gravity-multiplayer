extends RigidBody

var lifetime = 1
var speed = 50
var damage = 1
var direction
var shooter_id

onready var global = get_node("/root/global")
onready var area = $Area
onready var hit_sound = $AudioStreamPlayer3D

func _ready():
	area.connect("body_entered", self, "hit")
	direction = self.global_transform.basis.z
	global.call_delayed(self, "remove_self", lifetime)

func _process(delta):
	self.translation += direction * speed * delta

func hit(poor_thing):
	print("hit something")
	#hit_sound.play()
	if is_network_master():
		if not int(poor_thing.name) == int(shooter_id) and poor_thing.has_method("damage"):
			poor_thing.rpc_id(int(poor_thing.name), "damage", damage)
	self.queue_free()

func remove_self():
	self.queue_free()
