extends Node

onready var game_scene = load("res://scenes/game.tscn")
onready var music_scene = load("res://scenes/music.tscn")
onready var main_theme_stream = load("res://misc/audio/music/main_theme.wav")

var game
var music
var vr = false
var peer = 0

var default_username = "ProMinecraft123"
var network_interpolation_factor = 15




func _ready():
	print(" ")
	print("ready global.gd")
	music = music_scene.instance()
	self.add_child(music)
	if self.get_children().size() > 0:
		for child in self.get_children():
			if child.name == "music":
				self.music = child

func _process(delta):
	if Input.is_action_just_pressed("ui_home"):
		reload_game()
	
	if Input.is_action_just_released("ui_cancel"):
		print("killing game...")
		print(" ")
		get_tree().quit()




func delayexecute(object_reference, function_name, delay):
	var timer = Timer.new()
	timer.one_shot = true
	timer.wait_time = delay
	timer.connect("timeout", object_reference, function_name)
	self.add_child(timer)
	timer.start()




func reload_game():
	print("")
	print("reloading game...")
	print("")
	OS.window_fullscreen = false
	get_tree().reload_current_scene()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)




func rand_point_between_radiuses(rng, min_radius, max_radius):
	var random_rotated_vector = Vector3(1, 0, 0).rotated(Vector3(0, 1, 0), rng.randf_range(deg2rad(0), deg2rad(360)))
	var random_vec3 = random_rotated_vector * rng.randi_range(min_radius, max_radius)
	var random_point = Vector2(random_vec3.x, random_vec3.z) 
	return random_point




func call_delayed(object_reference, function_name, delay):
	var timer = Timer.new()
	timer.one_shot = true
	timer.wait_time = delay
	timer.connect("timeout", object_reference, function_name)
	self.add_child(timer)
	timer.start()




func check_vr(): #Enables VR if possible
	var first_arvr_interface = ARVRServer.get_interface(0)
	if first_arvr_interface and first_arvr_interface.initialize():
		get_viewport().arvr = true
		print("you are gaming in vr...")




func create_game():
	var game_scene_instance = game_scene.instance()
	self.add_child(game_scene_instance)
	if self.get_children().size() > 0:
		for child in self.get_children():
			if child.name == "game":
				self.game = child
