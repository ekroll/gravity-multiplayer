shader_type canvas_item;

uniform bool enable;
uniform bool enable_general_adjustments;
uniform float general_brightness;
uniform float general_contrast;
uniform float general_saturation;
uniform bool enable_vignette;
uniform float vignette_amount;
uniform bool enable_fake_hdr;



//Fake HDR overexposed
uniform float fake_hdr_overexposure;
uniform float fake_hdr_contrast_overexposed;
uniform float fake_hdr_saturation_overexposed;



//fake HDR underexposed
uniform float fake_hdr_underexposure;
uniform float fake_hdr_contrast_underexposed;
uniform float fake_hdr_saturation_underexposed;



uniform bool enable_contrast_modulation;
uniform vec3 contrast_modulation_color;
uniform float contrast_modulation_amount;

uniform bool enable_bloom;
uniform float bloom_treshold;


float avg_vec3_to_float(vec3 the_vector) {
	float the_float = (the_vector.x + the_vector.y + the_vector.z) / 3.0;
	return the_float;
}


void fragment() {
	vec2 texture_uv = SCREEN_UV;
	vec3 screen_texture = textureLod(TEXTURE, texture_uv, 10.0).rgb;
	
	
	if (enable) {
		if (enable_fake_hdr) {
			vec3 underexposed_screen_texture = screen_texture * fake_hdr_underexposure;
			underexposed_screen_texture = mix(vec3(0.5), underexposed_screen_texture, fake_hdr_contrast_underexposed);
			underexposed_screen_texture = mix(vec3(dot(vec3(1.0), underexposed_screen_texture) * 0.33333), underexposed_screen_texture, fake_hdr_saturation_underexposed);
	
	
	
			vec3 overexposed_screen_texture = screen_texture * fake_hdr_overexposure;
			overexposed_screen_texture = mix(vec3(0.5), overexposed_screen_texture, fake_hdr_contrast_overexposed);
			overexposed_screen_texture = mix(vec3(dot(vec3(1.0), overexposed_screen_texture) * 0.33333), overexposed_screen_texture, fake_hdr_saturation_overexposed);
	
	
	
			vec3 fake_hdr_screen_texture = mix(underexposed_screen_texture, overexposed_screen_texture, screen_texture);
			//screen_texture = (screen_texture + fake_hdr_screen_texture) / 2.0;
			screen_texture = mix(screen_texture, fake_hdr_screen_texture, 0.5);
		}
	
	
		if (enable_general_adjustments) {
			screen_texture *= general_brightness;
			
			if (enable_contrast_modulation) {
				screen_texture = mix(vec3(0.5) * normalize(contrast_modulation_color) * contrast_modulation_amount, screen_texture, general_contrast);
			}
			
			else {
				screen_texture = mix(vec3(0.5), screen_texture, general_contrast);
			}
			
			screen_texture = mix(vec3(dot(vec3(1.0), screen_texture) * 0.33333), screen_texture, general_saturation);
		}
	
	
		if (enable_vignette) {
			float dist = distance(SCREEN_UV, vec2(0.5)) * 2.0;																	//Calculating distance towards edge of screen
			dist *= vignette_amount + (COLOR.r + COLOR.g + COLOR.b) / 10.0;																														//Decreasing vignette effect a bit
			float vignette = 1.0 - dist;
			vignette = clamp(vignette, 0.0, 1.0);																								//Clamping vignetteeffect between 0 and 1
			screen_texture *= vignette;																													//Applying vignette
		} 
	}
	
	//for (int i=1; i<3; i++) {
    //    screen_texture +=
      //      texture2D(TEXTURE, (vec2(FRAGCOORD.xy) + vec2(0.0, offset[i])) / 1024.0)
        //        * weight[i];
        //FragmentColor +=
          //  texture2D(TEXTURE, (vec2(FRAGCOORD.xy) - vec2(0.0, offset[i])) / 1024.0)
            //    * weight[i];
	
	vec3 blurred_screen_texture = screen_texture;
	
	//X Direction
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(SCREEN_PIXEL_SIZE.x, 0.0)).xyz * 0.15;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(-SCREEN_PIXEL_SIZE.x, 0.0)).xyz * 0.15;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(2.0 * SCREEN_PIXEL_SIZE.x, 0.0)).xyz * 0.12;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(2.0 * -SCREEN_PIXEL_SIZE.x, 0.0)).xyz * 0.12;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(3.0 * SCREEN_PIXEL_SIZE.x, 0.0)).xyz * 0.09;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(3.0 * -SCREEN_PIXEL_SIZE.x, 0.0)).xyz * 0.09;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(4.0 * SCREEN_PIXEL_SIZE.x, 0.0)).xyz * 0.05;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(4.0 * -SCREEN_PIXEL_SIZE.x, 0.0)).xyz * 0.05;
	
	//Y Direction
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(0.0, SCREEN_PIXEL_SIZE.y)).xyz * 0.15;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(0.0, -SCREEN_PIXEL_SIZE.y)).xyz * 0.15;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(0.0, 2.0 * SCREEN_PIXEL_SIZE.y)).xyz * 0.12;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(0.0, 2.0 * -SCREEN_PIXEL_SIZE.y)).xyz * 0.12;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(0.0, 3.0 * SCREEN_PIXEL_SIZE.y)).xyz * 0.09;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(0.0, 3.0 * -SCREEN_PIXEL_SIZE.y)).xyz * 0.09;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(0.0, 4.0 * SCREEN_PIXEL_SIZE.y)).xyz * 0.05;
	blurred_screen_texture += texture(TEXTURE, SCREEN_UV + vec2(0.0, 4.0 * -SCREEN_PIXEL_SIZE.y)).xyz * 0.05;
	
	vec3 bloom_texture;
	
	if (enable_bloom) {
		bloom_texture = clamp(blurred_screen_texture - bloom_treshold, 0, 16);
		screen_texture += bloom_texture;
	}
	
	COLOR.rgb = screen_texture;
	//COLOR.rg = texture_uv;
	//COLOR.b = 0.0;
}