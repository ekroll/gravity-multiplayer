extends Node2D

onready var global = get_node("/root/global")
onready var server = get_node("/root/server")
onready var username_text_edit = $username_text_edit
onready var ip_text_edit = $ip_text_edit
onready var port_text_edit  = $port_text_edit 
var default_ip = "localhost"

func _ready():
	print("ready client.gd")
	self.hide()
	
	username_text_edit.text = str(global.default_username)
	ip_text_edit.text = str(default_ip)
	port_text_edit.text = str(server.default_port)
	
	get_tree().connect("connected_to_server", self, "connected_to_server")
	get_tree().connect("connection_failed", self, "failed_to_connect")
	get_tree().connect("server_disconnected", self, "server_disconected")

func _on_join_game(): #The function that is called when pressing "Join Game" as client.
	self.visible = false
	global.create_game()
	make_client(ip_text_edit.text, int(port_text_edit.text))
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	print("connection_status: ", global.peer.get_connection_status(), ", (1 and 2 are nice numbers)")
	if global.peer.get_connection_status() == 0:
		failed_to_connect()
	else:
		OS.window_fullscreen = true

func make_client(ip, port): #EZ-client-creator, like start_server() above
	global.peer = NetworkedMultiplayerENet.new()
	global.peer.create_client(ip, port)
	print("creating client at ", ip, " and port ", port)
	get_tree().set_network_peer(global.peer)
	return global.peer

func connected_to_server():
	print("welcome player, you have joined the game...")
	global.game.world.rpc_id(1, "request_world", get_tree().get_network_unique_id())
	global.game.world.rpc("add_new_player", get_tree().get_network_unique_id())
	global.game.world.rpc_id(1, "get_existing_players", get_tree().get_network_unique_id())
	global.game.world.rpc_id(1, "request_playerbuilt_objects")

func failed_to_connect():
	print("failed to connect to server")
	global.reload_game()

func server_disconnected():
	print("server disconnected")
	global.reload_game()
