extends Spatial

var map_size = Vector2(100, 100)
var barrier_height = 25

onready var negative_z_barrier_side = $"-z_barrier_side"
onready var positive_z_barrier_side = $z_barrier_side
onready var negative_x_barrier_side = $"-x_barrier_side"
onready var positive_x_barrier_side = $x_barrier_side
onready var top_barrier = $top_barrier

func setup(nothing):
	negative_z_barrier_side.barrier_width = map_size.x + 2
	negative_z_barrier_side.barrier_depth = map_size.y + 1
	positive_z_barrier_side.barrier_width = map_size.x + 2
	positive_z_barrier_side.barrier_depth = map_size.y + 1
	top_barrier.barrier_width = map_size.y
	top_barrier.barrier_depth = barrier_height

	negative_x_barrier_side.barrier_width = map_size.y + 2
	negative_x_barrier_side.barrier_depth = map_size.x + 1
	positive_x_barrier_side.barrier_width = map_size.y + 2
	positive_x_barrier_side.barrier_depth = map_size.x + 1

	negative_z_barrier_side.barrier_height = barrier_height
	positive_z_barrier_side.barrier_height = barrier_height
	negative_x_barrier_side.barrier_height = barrier_height
	positive_x_barrier_side.barrier_height = barrier_height
	top_barrier.barrier_height = map_size.x
	

	negative_z_barrier_side.setup()
	positive_z_barrier_side.setup()
	negative_x_barrier_side.setup()
	positive_x_barrier_side.setup()
	top_barrier.setup()
	return
