extends Popup

var popped_up = false
var toggle_function_name = "property_toggled"
var dropdown_function_name = "property_set"

#Keep things in order. Index order in dropdown_properties should align with index order in dropdown_options
var dropdown_properties = ["rendering/quality/filters/anisotropic_filter_level", 
							"rendering/quality/directional_shadow/size"]
var anisotropic_filter_level_options = [1, 2, 4, 8, 16]
var directional_shadow_size_options = [1024, 2048, 4096]
var dropdown_options = [anisotropic_filter_level_options, directional_shadow_size_options]

#Just to keep things simple, i made a separate array for toggle_properties so i wouldnt have to create empty indexes in the dropdown_properties array for toggle_properties without options (just true or false)
var toggle_properties = ["rendering/quality/reflections/texture_array_reflections", 
							"rendering/quality/shading/force_vertex_shading"]
##############################################################
onready var tab_window = $TabWindow
onready var grid_container = $TabWindow/Graphics/ScrollContainer/GridContainer
onready var toggle_property_edit = load("res://scenes/interactive_things/ui/toggle_property_edit.tscn")
onready var dropdown_property_edit = load("res://scenes/interactive_things/ui/dropdown_property_edit.tscn")


func property_toggled(property_name, toggle_state):
	if ProjectSettings.has_setting(property_name):
		ProjectSettings.set_setting(property_name, toggle_state)
		ProjectSettings.save()
		print("Toggled ProjectSetting ", property_name, " to ", toggle_state)
	else:
		print("ProjectSetting ", property_name, " does not exist")
	
func property_set(property_name, property_value):
	if ProjectSettings.has_setting(property_name):
		ProjectSettings.set_setting(property_name, property_value)
		ProjectSettings.save()
		print("Changed ProjectSetting ", property_name, " to ", property_value)
	else:
		print("ProjectSetting ", property_name, " does not exist")

func _ready():
	for toggle_property in toggle_properties:
		var new_toggle_property_edit = toggle_property_edit.instance()
		new_toggle_property_edit.name = toggle_property.replace("/", " -> ")
		grid_container.add_child(new_toggle_property_edit)
	
	for dropdown_property in dropdown_properties:
		var new_dropdown_property_edit = dropdown_property_edit.instance()
		new_dropdown_property_edit.name = dropdown_property.replace("/", " -> ")
		new_dropdown_property_edit.options = dropdown_options[dropdown_properties.find(dropdown_property)]
		grid_container.add_child(new_dropdown_property_edit)
	
	for property_edit in grid_container.get_children():
		property_edit.connect(toggle_function_name, self, toggle_function_name)
		property_edit.connect(dropdown_function_name, self, dropdown_function_name)

func _process(delta):
	self.rect_size = OS.window_size
	if Input.is_action_just_pressed("options"):
		popped_up = !popped_up
	if popped_up:
		self.popup()
	else:
		self.hide()
		
