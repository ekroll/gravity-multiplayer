extends KinematicBody

#Interesting variables
var min_health = 0
var max_health = 5
var walk_speed = 2
var sprint_speed = 4
var jump_force = 7.5
var gravity = 9.8
var visible_distance = 10000
var mouse_sensitivity = Vector2(0.002, 0.002)
var controller_sensitivity = Vector2(0.05, 0.04)
var controller_deadzone = 0.25


#Global value holders
var team
var speed
var health = max_health
var pitch = 0
var net_id
var raycast_distance = 0
var velocity = Vector3()
var up = Vector3(0, 1, 0)
var current_structure
var inv_gravity_interp_speed
var default_fov
var underwater_fov

#Constants
const NORMAL_HEAD_ROT = 0
const INV_GRAVITY_HEAD_ROT = -180
const INV_GRAVITY_INTERP_SPEED = 7
const TIMEOUT_TIME_SCALE = 0.05
const NORMAL_TIME_SCALE = 1

#Checkers
var inverse_gravity = false
var low_graphics = false
var timeout = false
var is_on_ground = true
var is_sprinting = false
var place_objects = false
var is_placing_object = false
var build_structure = false

#Not so interesting variables
onready var camera = $HeadPivotPoint/ARVRCamera
onready var raycast = $HeadPivotPoint/ARVRCamera/raycast
onready var head_pivot = $HeadPivotPoint
onready var head_pivot_loc_1 = $HeadPivotPointPosition_1.translation
onready var head_pivot_loc_2 = $HeadPivotPointPosition_2.translation
onready var body_mesh = $CollisionShape/CapsuleMesh
onready var collision_shape = $CollisionShape
onready var left_hand = $HeadPivotPoint/ARVRCamera/left_hand
onready var right_hand = $HeadPivotPoint/ARVRCamera/right_hand
onready var jetpack_sound = $HeadPivotPoint/Jetpack_sound
onready var wind_sound = $HeadPivotPoint/ARVRCamera/Head/wind_sound
onready var subsea_sound = $HeadPivotPoint/ARVRCamera/Head/subsea_sound
onready var spawn_sound = $HeadPivotPoint/ARVRCamera/Head/spawn_sound
onready var spawn_particles = $spawn_particles
onready var hp_bar = $HUD/hp_bar
onready var default_environment = load("res://resources/environments/default_env.tres")
onready var underwater_environment = load("res://resources/environments/underwater_env.tres")
onready var global = get_node("/root/global")

#Network related variables
puppet var puppet_translation = Vector3()
puppet var puppet_head_pivot_rotation = Vector3()
puppet var puppet_head_pivot_translation = Vector3()

#script-start of player_character
func _ready():
	wind_sound.unit_db = -80
	if is_network_master():
		global.game.toggle_low_graphics()
		global.game.toggle_low_graphics()
		inv_gravity_interp_speed = INV_GRAVITY_INTERP_SPEED
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		camera.environment = default_environment
		default_fov = global.game.fov
		camera.fov = default_fov
		camera.far = visible_distance
		hp_bar.min_value = 0
		hp_bar.max_value = max_health
		hp_bar.value = max_health
		spawn_sound.play()

#a function to easily return a vector pointing in the direction you want to move
func get_movement_input():
	var input_dir = Vector3()
	if Input.is_action_pressed("forward"):
		input_dir += Vector3(0, 0, -1).rotated(Vector3(0, 1, 0), head_pivot.rotation.y)
	if Input.is_action_pressed("backwards"):
		input_dir += Vector3(0, 0, 1).rotated(Vector3(0, 1, 0), head_pivot.rotation.y)
	if Input.is_action_pressed("left"):
		input_dir += -camera.global_transform.basis.x
	if Input.is_action_pressed("right"):
		input_dir += camera.global_transform.basis.x
	input_dir = input_dir.normalized()
	return input_dir

#Godots built-in function I am using to do most of my input stuff
func _input(event):
	if (is_network_master()):
		if event is InputEventKey:
			if int(OS.get_scancode_string(event.scancode)) != 0: #Lazy way to get a build_object index to retrieve an object from
				if event.pressed:
					print("pressed key ", OS.get_scancode_string(event.scancode))
					var index = int(OS.get_scancode_string(event.scancode)) - 1
					if current_structure != null:
						current_structure.enable_collision()
					add_structure(index)
					is_placing_object = true
					event.set_pressed(false)

		#Moving the added object to the raycast collission point (where you are looking)
		if is_placing_object:
			move_structure(raycast.get_collision_point())

		#Rotating the added object 90 degrees in a positive direction
		if event is InputEventMouseButton:
			if event.is_pressed():
				if event.button_index == BUTTON_WHEEL_UP:
					if is_placing_object:
						if current_structure != null:
							rotate_structure(1)

				#Rotating the added object 90 degrees in a negative direction
				if event.button_index == BUTTON_WHEEL_DOWN:
					if is_placing_object:
						if current_structure != null:
							rotate_structure(-1)

		#Code to confirm that you want to build the currently added object
		if Input.is_action_just_pressed("LMouse") or Input.is_action_pressed("L2"):
			build_structure = true

		#Code for looking around with camera using a mouse
		if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			pitch = clamp(pitch - event.relative.y * mouse_sensitivity.y, -PI/2, PI/2)
			head_pivot.rotation.x = pitch
			head_pivot.rotation.y -= event.relative.x * mouse_sensitivity.x

		#Code for processing raw joystick input into nice usable stuff
		var stick_input = Vector2(Input.get_joy_axis(0, JOY_AXIS_2), Input.get_joy_axis(0, JOY_AXIS_3))
		var magnitude = sqrt(stick_input.x * stick_input.x + stick_input.y * stick_input.y)
		if(magnitude < controller_deadzone):
			stick_input = Vector2(0,0)
		else:
			#Controller code to look around with camera
			stick_input = stick_input.normalized() * ((magnitude - controller_deadzone) / (1 - controller_deadzone))
			head_pivot.rotation.x -= stick_input.y * controller_sensitivity.y
			head_pivot.rotation.x = clamp(head_pivot.rotation.x, -PI/2, PI/2)
			head_pivot.rotation.y -= stick_input.x * controller_sensitivity.x

		#VR only code to rotate camera 90 degrees with right stick (if it isnt possible to turn with your head)
		if (global.vr):
			if Input.is_action_just_pressed("turn_left"):
				head_pivot.rotation.y = head_pivot.rotation.y + PI/4
			if Input.is_action_just_pressed("turn_right"):
				head_pivot.rotation.y = head_pivot.rotation.y - PI/4

		#Code to slow down your local game time (fancy timeout, might be removed)
		if Input.is_action_just_pressed("toggle_time"):
				toggle_timeout()
		if timeout:
			Engine.time_scale = TIMEOUT_TIME_SCALE
		else:
			Engine.time_scale = NORMAL_TIME_SCALE

		#Code to set custom respawn point and saving it in global
		if Input.is_action_just_pressed("set_respawn_point"):
			global.game.world.respawn_point = self.translation

#One of Godot`s looping functions
func _process(delta):

	#Code to check if you are actually playing this character, or if it is a local network-copy of your player on someone elses computer
	if is_network_master():
		hp_bar.value = health
		global.game.world.sun.visible = !is_underwater()
		
		#Code to avoid weird camera issues, such as switching camera with other players when joining. please find another way to do this
		camera.make_current()
		underwater_fov = default_fov * (1 - 0.33)
		#Checking if you are submerged, doing stuff based on that
		if is_underwater():
			camera.fov = underwater_fov
			camera.environment = underwater_environment
		else:
			camera.fov = default_fov
			camera.environment = default_environment
			var dof_distance = self.translation.distance_to(raycast.get_collision_point())
			dof_distance = dof_distance + (0 - dof_distance) * delta
			set_dof_distance(dof_distance)

		#Setting volume of the wind based on your speed
		wind_sound.unit_db = clamp(2 * self.velocity.length() / 1 - 60, -60, 0)
		#Muting buses and effects based on if you are under water or not
		AudioServer.set_bus_mute(4, is_underwater())
		AudioServer.set_bus_mute(5, not is_underwater())
		AudioServer.set_bus_effect_enabled(0, 0, is_underwater())

		#Code to show and hide options (when it is ready)
		if Input.is_action_just_pressed("set_respawn_point"):
			global.game.world.set_respawn_point(self.translation)

		if Input.is_action_just_pressed("toggle_low_graphics"):
			global.game.toggle_low_graphics()

			#Code to interact with whatever you are holding in your left hand (both vr and normal) # the code I will use to impoement Grappler soon
		#if Input.is_action_just_pressed("LMouse")  or Input.is_action_pressed("L2"):
			#var held_object = left_hand.get_child(0)
			#if held_object.has_method("interact"):
				#held_object.rpc("interact", self, get_tree().get_network_unique_id())

		#same as above, but with right hand
		if Input.is_action_pressed("RMouse")  or Input.is_action_pressed("R2"):
			var held_object = right_hand.get_child(0)
			if held_object.has_method("interact"):
				held_object.rpc("interact", self, get_tree().get_network_unique_id())

		#Updating the translation, rotation and so on, to all the other players local copy of your player
		rset_unreliable("puppet_translation", self.translation)
		rset_unreliable("puppet_head_pivot_rotation", head_pivot.rotation)
		rset_unreliable("puppet_head_pivot_translation", head_pivot.translation)
	else:
		#Applying translation, rotation and so on if this scene is a local "network copy" of your player
		self.translation = self.translation + (puppet_translation - self.translation) * delta * global.network_interpolation_factor
		head_pivot.rotation = head_pivot.rotation + (puppet_head_pivot_rotation - head_pivot.rotation) * delta * global.network_interpolation_factor
		head_pivot.translation = head_pivot.translation + (puppet_head_pivot_translation - head_pivot.translation) * delta * global.network_interpolation_factor

#Another of Godot`s looping functions, specifically made for physics
func _physics_process(delta):
	if is_network_master():

		#Physics related stuff if you are submerged or not
		var movement_velocity
		if is_underwater():
			#Makes you move downward relative to you
			if Input.is_action_pressed("sprint"):
				velocity.y = -jump_force / 2.0

			#Makes it so you dont have to hold shift to "run" underwater, and since it also would have moved you downwards
			movement_velocity = get_movement_input() * sprint_speed * 1.33
			#Some fake linear-dampening
			velocity = velocity.linear_interpolate(Vector3(0, 0, 0), delta * 2)
		else:
			#Deals with sprinting
			if Input.is_action_pressed("sprint"):
				speed = sprint_speed
			else:
				speed = walk_speed
			movement_velocity = get_movement_input() * speed

			if !is_on_floor():
				velocity.y -= gravity * delta

		#Decides when you can jump
		if is_on_floor():
			if Input.is_action_pressed("jump"):
				velocity.y = jump_force
		
		#Or swim...
		if is_underwater():
			if Input.is_action_pressed("jump"):
				velocity.y = jump_force / 2.0

		#Applies all movement
		velocity.x = movement_velocity.x
		velocity.z = movement_velocity.z
		velocity = move_and_slide(velocity, up)

		#Inverts gravity and deals with neccessary stuff that happens during the process
		if Input.is_action_just_pressed("invert_gravity"):
			self.invert_gravity()
		if inverse_gravity:
			head_pivot.translation = head_pivot.translation + (head_pivot_loc_2 - head_pivot.translation) * delta * inv_gravity_interp_speed
			head_pivot.rotation_degrees.z = head_pivot.rotation_degrees.z + (INV_GRAVITY_HEAD_ROT - head_pivot.rotation_degrees.z) * delta * inv_gravity_interp_speed
		else:
			head_pivot.translation = head_pivot.translation + (head_pivot_loc_1 - head_pivot.translation) * delta * inv_gravity_interp_speed
			head_pivot.rotation_degrees.z = head_pivot.rotation_degrees.z + (NORMAL_HEAD_ROT - head_pivot.rotation_degrees.z) * delta * inv_gravity_interp_speed

#A function to easily inflict damage to this player (things that damages calls this function)
remote func damage(damage):
	self.health -= damage
	if self.health < 1:
		self.die()

#Code to make the timeout thing happen
sync func toggle_timeout():
	timeout = !timeout

#The function that inverts gravity and the other stuff
func invert_gravity():
	inverse_gravity = !inverse_gravity
	controller_sensitivity *= -1
	jump_force = -jump_force
	mouse_sensitivity *= -1
	gravity *= -1
	up = -up

#The function that makes you die
func die():
	health = max_health
	self.translation = global.game.world.respawn_point

#The function to check if you are submerged
func is_underwater():
	var is_underwater
	if inverse_gravity:
		if (self.translation.y > -0.153): #the first one
			is_underwater = false
		else:
			is_underwater = true

	else:
		if (self.translation.y < -0.882): #the first one minus 0.729
			is_underwater = true
		else:
			is_underwater = false

	return is_underwater

#Function to add local object
func add_structure(structure_index):
	build_structure = false
	if global.game.world.structures.size() > structure_index:
		var new_structure = global.game.world.structures[structure_index].instance()
		new_structure.index = structure_index
		global.game.world.player_structures.add_child(new_structure)
		current_structure = global.game.world.playerbuilt_objects.get_children().back()
		if current_structure.has_method("disable_collision"):
			current_structure.disable_collision()
		print("added object ", current_structure, " to the world")
	else:
		print("could not add object")

#Function to move local object
func move_structure(new_translation):
	if current_structure != null:
		if !build_structure:
			current_structure.translation = Vector3(float(int(new_translation.x)), float(int(new_translation.y)), float(int(new_translation.z)))
		else:
			current_structure.enable_collision()
			global.game.world.rpc("build_structure", current_structure.index, current_structure.translation, current_structure.rotation)
			is_placing_object = false
	else:
		is_placing_object = false
		build_structure = false

#Function to rotate local object 90 degrees in positive or negative direction
func rotate_structure(positive_or_negative):
	if positive_or_negative > 0:
		current_structure.rotation_degrees.y += 90
	if positive_or_negative < 0:
		current_structure.rotation_degrees.y -= 90

#Function to set DOF stuff that looks nice in-game
func set_dof_distance(distance):
	camera.environment.dof_blur_near_distance = distance
	camera.environment.dof_blur_near_transition = distance
	camera.environment.dof_blur_far_distance = distance
	camera.environment.dof_blur_far_distance = distance
	camera.environment.dof_blur_far_transition = distance
