shader_type spatial;
render_mode specular_toon, ensure_correct_normals, cull_disabled;

uniform vec2 sea_size_meters;
uniform vec3 sea_color;
uniform vec3 sss_color;
uniform float amplitude;
uniform float wave_sharpness;
uniform float merkyness;

uniform sampler2D small_noise;
uniform sampler2D small_normalmap;
uniform float small_texture_size;
uniform vec2 small_texture_offset_1;
uniform vec2 small_texture_offset_2;

uniform sampler2D medium_noise;
uniform sampler2D medium_normalmap;
uniform float medium_texture_size;
uniform vec2 medium_texture_offset_1;
uniform vec2 medium_texture_offset_2;

uniform sampler2D large_noise;
uniform sampler2D large_normalmap;
uniform float large_texture_size;
uniform vec2 large_texture_offset_1;
uniform vec2 large_texture_offset_2;

varying float vertex_height;
varying vec3 disp_texture;
varying vec2 sea_uv;

void vertex() {
	disp_texture = VERTEX;
	vec2 sea_size = sea_size_meters / vec2(2.5);
	sea_uv = UV * sea_size;
	
	//small waves
	vec3 small_height1 = texture(small_noise, sea_uv / vec2(small_texture_size)  + small_texture_offset_1 * TIME * 0.045).rgb * 0.02;
	vec3 small_height2 = texture(small_noise, sea_uv / vec2(small_texture_size) + small_texture_offset_2 * TIME * 0.025).rgb * 0.02;
	disp_texture += 3.5 * (small_height1 + small_height2); 
	
	//medium waves
	vec3 medium_height1 = texture(medium_noise, sea_uv / vec2(medium_texture_size) + medium_texture_offset_1 * TIME * 0.075).rgb * 0.02;
	vec3 medium_height2 = texture(medium_noise, sea_uv / vec2(medium_texture_size) + medium_texture_offset_2 * TIME * 0.055).rgb * 0.02;
	disp_texture += 7.5 * (medium_height1 + medium_height2); 
	
	//large waves
	vec3 large_height1 = texture(large_noise, sea_uv / vec2(large_texture_size) + large_texture_offset_1 * TIME * 0.005).rgb * 0.02;
	vec3 large_height2 = texture(large_noise, sea_uv / vec2(large_texture_size) + large_texture_offset_2 * TIME * 0.015).rgb * 0.02;
	disp_texture += 35.0 * (large_height2 + large_height2); 
	
	disp_texture /= 3.0;
	disp_texture *= amplitude;
	disp_texture -= amplitude / 2.0;
	disp_texture = vec3((disp_texture.r + disp_texture.g + disp_texture.b) / 3.0 - (sea_uv.r + sea_uv.g) * 1.4 + (sea_size.x + sea_size.y) / 1.4);
	disp_texture = vec3(pow((disp_texture.r + 1.0) / 2.0, wave_sharpness) + pow((disp_texture.g + 1.0) / 2.0, wave_sharpness) + pow((disp_texture.b + 1.0) / 2.0, wave_sharpness));
	
	//VERTEX += disp_texture;
	VERTEX.x += (disp_texture.r - 0.5);
	VERTEX.y += (disp_texture.g - 0.5);
	VERTEX.z += (disp_texture.b - 0.5);
}

void fragment() {
	//offsetting normalmaps according to vertex displacement
	vec3 small_combined_normalmap = (texture(small_normalmap, sea_uv / vec2(small_texture_size) + small_texture_offset_1 * TIME * 0.045).rgb 
								 								+ texture(small_normalmap, sea_uv / vec2(small_texture_size) + small_texture_offset_2 * TIME * 0.025).rgb) / 2.0;
								 
	vec3 medium_combined_normalmap = (texture(medium_normalmap, sea_uv / vec2(medium_texture_size) + medium_texture_offset_1 * TIME * 0.075).rgb 
								 									+ texture(medium_normalmap, sea_uv / vec2(medium_texture_size) + medium_texture_offset_2 * TIME * 0.055).rgb) / 2.0;
								
	vec3 large_combined_normalmap = (texture(large_normalmap, sea_uv / vec2(large_texture_size) + large_texture_offset_1 * TIME * 0.005).rgb 
								 								+ texture(large_normalmap, sea_uv / vec2(large_texture_size) + large_texture_offset_2 * TIME * 0.015).rgb) / 2.0;
	vec3 normalmap = (small_combined_normalmap + medium_combined_normalmap + large_combined_normalmap) / 3.0;
	vec3 normal = (TANGENT * normalmap.y + BINORMAL * normalmap.x + NORMAL * normalmap.z) / 3.0;
	
	vec2 refraction_uv = SCREEN_UV - normal.xy * dot(disp_texture, vec3(1, 0, 0)) * vec2(0.01);
	//vec2 ref_ofs = SCREEN_UV - ref_normal.xy * dot(texture(texture_refraction,base_uv),refraction_texture_channel) * refraction;
	
	float depth = texture(DEPTH_TEXTURE, refraction_uv).r;
				depth = depth * 2.0 - 1.0;
				depth = PROJECTION_MATRIX[3][2] / (depth + PROJECTION_MATRIX[2][2]);
				depth = depth + VERTEX.z;
				depth = exp(-depth * merkyness);
		vec4 upos = INV_PROJECTION_MATRIX * vec4(SCREEN_UV * 2.0 - 1.0, depth * 2.0 - 1.0, 1.0);
vec3 pixel_position = upos.xyz / upos.w;
				depth = clamp(depth, 0.0, 1.0);
	
	float fresnel = 1.0 - dot(VIEW, NORMAL);
	
	vec3 refraction_texture = textureLod(SCREEN_TEXTURE, refraction_uv, 0.0).rgb;
	refraction_texture *= normalize(sea_color);
	refraction_texture *= depth;
	
	//applying stuff
	vec3 processed_disp_texture = vec3(clamp(pow(1.0 - disp_texture.r, 2.0), 0.0, 1.0), clamp(pow(1.0 - disp_texture.g, 2.0), 0.0, 1.0), clamp(pow(1.0 - disp_texture.b, 2.0), 0.0, 1.0));
	ALBEDO =  sea_color * processed_disp_texture;
	ALBEDO = mix(refraction_texture, sea_color, pow(fresnel, 3.0));
	ALBEDO = mix(ALBEDO, sss_color, dot(normal, vec3(0, 1, 0)) * 0.017);

	//ALBEDO = mix(ALBEDO, sss_color, sss_color_mix_factor);
	//ALBEDO = normal;
	//ALBEDO = refraction_texture;
	//ALBEDO = vec3(depth);
	//ALBEDO = vec3(pow(fresnel, 3.0));
	//ALBEDO = mix(vec3(1.0), ALBEDO, clamp(smoothstep(pixel_position.z + 0.5, pixel_position.z, VERTEX.z), 0.0,1.0));
	ROUGHNESS = 0.0;
	METALLIC = 1.0;
	SPECULAR = 1.0;
	NORMALMAP = normalmap;
	NORMALMAP_DEPTH = 5.5;
	//SPECULAR = clamp(SPECULAR - depth * 10.0, 0.0, 1.0);
	//ALPHA = mix(1.0, 0, depth);
	//ALPHA = mix(1.0, ALPHA, clamp(smoothstep(pixel_position.z+0.5,pixel_position.z,VERTEX.z),0.0,1.0));
}