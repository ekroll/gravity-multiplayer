extends Node
onready var global = get_node("/root/global")

#BORING VARS
onready var generator = $gravity_world_generator
onready var sun = $sun

onready var players = $players
onready var player_structures = $player_structures
onready var world_objects = $world_objects
onready var paintballs = $paintballs

onready var player_character = load("res://scenes/3D/player_character/player_character.tscn")
onready var structures = {0: load("res://scenes/3D/build_tool_objects/large_stair.tscn"),
																1: load("res://scenes/3D/build_tool_objects/wall.tscn"),
																2: load("res://scenes/3D/build_tool_objects/floor.tscn"),
																3: load("res://scenes/3D/build_tool_objects/meduim_box.tscn"),
																4: load("res://scenes/3D/build_tool_objects/large_box.tscn"),
																5: load("res://scenes/3D/build_tool_objects/container.tscn")}
var spawn_point = Vector3(0, 180, 0)
var respawn_point = Vector3(0, 1, 0)

#STARTUP
func _ready():
	print("ready gravity_world.gd")




#A lot of other functions
sync func add_new_player(id): #The function used to add a player on every connected clients local games
	if not id in get_player_names():
		create_player_character(id)
		print("added player ", id)




func create_player_character(id):
	print("------------creating-player------------")
	
	var new_player = player_character.instance()
	new_player.set_name(str(id))
	print("instanced new character for player ", id)
	
	new_player.net_id = id
	print("named ", str(id), " after id ", str(id))
	
	new_player.set_network_master(int(id))
	print("gave ", id, " power over himself")
	
	new_player.translation = spawn_point
	players.add_child(new_player)
	print("player ", id, " spawned at", new_player.translation, "in the multiverse")
	
	if int(id) == get_tree().get_network_unique_id():
		print("player ", id, " is you")
	return player_character




func get_player_names(): #Gets names of all players
	var player_names = {}
	for player in players.get_children():
		player_names[int(player.name)] = str(player.name)
	return player_names




remote func get_existing_players(id): #Function the clients calls on server`s gamenode that sends an array with all the players back with add_existing_players()
	if get_tree().is_network_server():
		var existing_players = []
		for player in players.get_children():
			if player.name != str(id):
				var player_id = int(player.name)
				existing_players.append(player_id)
				print("sending existing players: ", existing_players, " to client ", id)
		rpc_id(id, "add_existing_players", existing_players)
	else:
		print("cant send players since i am not the server mr. ", id)




remote func add_existing_players(existing_players): #Function that adds all the existing characters to the local game with the array from previous
	if existing_players != null:
		print("received existing players: ", existing_players, " from id ", get_tree().get_rpc_sender_id())
		for player_id in existing_players:
			create_player_character(player_id)
	else:
		print("didnt receive any valid existing players")




remote func build_structure(structure_index, structure_translation, structure_rotation):
	print("building object from index: ", structure_index, " at ", structure_translation, " with rotation ", structure_rotation)
	var new_structure = structures[int(structure_index)].instance()
	new_structure.translation = structure_translation
	new_structure.rotation = structure_rotation
	player_structures.add_child(new_structure)
	print("enabled collision")




remote func request_playerbuilt_objects(id):
	if player_structures.get_children().size() != 0:
		for object in player_structures.get_children():
			rpc_id(id, "build_object", object.index, object.translation, object.rotation)




remote func request_world(id): #The clients function to get the world. Called on the server
	if get_tree().is_network_server():
		generator.rpc_id(id, "generate_world", generator.world_seed)
		print("sent world_seed ", generator.world_seed, " to player ", id)



