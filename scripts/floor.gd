extends Spatial

var index

func disable_collision():
	$floor_mesh/StaticBody/CollisionShape.disabled = true

func enable_collision():
	$floor_mesh/StaticBody/CollisionShape.disabled = false