extends Spatial

onready var global = get_node("/root/global")
onready var collision_shape = $barrier_side_mesh/collision_tester/collision_shape
onready var barrier_side_mesh = $barrier_side_mesh
var barrier_depth = 100
var barrier_width = 100
var barrier_height = 600

func setup():
	print("setting up barrier side")
	barrier_side_mesh.mesh.size = Vector2(barrier_width, barrier_height)
	print("set barrier size to ", barrier_width, "m wide and ", barrier_height, "m, tall")
	collision_shape.shape.extents = Vector3(barrier_width / 2.0, barrier_height / 2.0, 1.0)
	print("set barrier collider size to ", barrier_width, "m wide and ", barrier_height, "m, tall")
	barrier_side_mesh.mesh.subdivide_width = int(barrier_width / 8)
	barrier_side_mesh.mesh.subdivide_depth = int(barrier_height / 8)
	print("subdivided barier_mesh to ", Vector2(barrier_width / 8, barrier_height / 8))
	self.translation.y = barrier_height / 2
	barrier_side_mesh.translation.z = -barrier_depth / 2
	print("moved barriers up to the edge of the pool")

func on_prison_break(body):
	if str(body.get_class()) == "KinematicBody":
		body.die()
		print("ah you noob ", body.name, ", you thought you could escape?")
