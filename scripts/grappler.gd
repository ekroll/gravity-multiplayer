extends Spatial
var can_shoot = true
var fire_rate = 0.0001
var grapple_length = 2500000000000000

onready var aimball = $aimball

func _ready():
	pass
########################VARS AND FUNCS########################
onready var root = get_node("/root")
onready var nootball = load("res://scenes/paintball.tscn")
onready var bullet_exit = $gun_mesh/bullet_exit
onready var gunshot_sound = $gunshot_sound
onready var raycast = $RayCast

sync func interact(shooter_id, character, team):
	global.execute_function_after_delay(self, "can_shoot", fire_rate)
	if raycast.is_colliding():
		if self.global_transform.origin.distance_to(raycast.get_collision_point()) < grapple_length:
			if can_shoot:
				gunshot_sound.play()
				var grapple_point = raycast.get_collision_point()
				var impulse = grapple_point - character.translation
				character.velocity = impulse * 2
				can_shoot = false

func can_shoot():
	can_shoot = true
	
func _process(delta):
	#if raycast.is_colliding():
		#aimball.global_transform.origin = raycast.get_collision_point()
		#aimball.visible = true
	#else:
		#aimball.visible = false
		pass
	
