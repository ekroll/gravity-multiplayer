extends Spatial

var amount_of_rocks = 100
var exents = Vector3(100, -50, 100)
var rock_scale = 10

onready var global = get_node("/root/global")
var rock = load("res://scenes/3D/gravity_world/generator/floating_rock.tscn")
var rng = RandomNumberGenerator.new()

func _ready():
	self.translation.x = -exents.x / 2
	self.translation.z = -exents.z / 2

func generate(rock_generator_seed):
	print("generating rocks")
	print("verify rock seed: ", rock_generator_seed)
	rng.set_seed(rock_generator_seed)
	for every_rock in range(0, amount_of_rocks):
		var new_rock = rock.instance()
		var random_rock_translation = Vector3(rng.randi_range(1, exents.x), rng.randi_range(exents.y + 10, -5), rng.randi_range(1, exents.z))
		print(random_rock_translation)
		new_rock.translation = random_rock_translation
		self.add_child(new_rock)
	return
