extends Spatial

const CHUNK_SIZE = 64
const CHUNK_RENDER_DISTANCE = 16
const TERRAIN_NOISE_DETAIL = 8

var player_translation = Vector3(0, 0, 0)
var terrain_noise
var thread

var chunks = {}
var unready_chunks = {}

 
onready var chunk = load("res://scenes/3D/gravity_world/generator/dimensions/infinite_world/chunk.tscn")

func _ready():
	randomize()
	terrain_noise = OpenSimplexNoise.new()
	terrain_noise.seed = randi()
	terrain_noise.octaves = TERRAIN_NOISE_DETAIL
	terrain_noise.period = 80
	
	thread = Thread.new()

func add_chunk(x, z):
	var key = str(x) + "," + str(z)
	if chunks.has(key) or unready_chunks.has(key):
		return
	if !thread.is_active():
		thread.start(self, "load_chunk", [thread, x, z])
		unready_chunks[key] = 1

func load_chunk(array):
	thread = array[0]
	var x = array[1]
	var z = array[2]
	
	var chunk = Chunk.new(x * CHUNK_SIZE, z * CHUNK_SIZE, CHUNK_SIZE, TERRAIN_NOISE_DETAIL, terrain_noise)
	chunk.translation = Vector3(x * CHUNK_SIZE, 0, z * CHUNK_SIZE)
	
	call_deferred("load_done", chunk, thread)

func load_done(chunk, thread):
	self.add_child(chunk)
	var key = str(chunk.x / CHUNK_SIZE) + "," + str(chunk.z / CHUNK_SIZE)
	chunks[key] = chunk
	unready_chunks.erase(key)
	thread.wait_to_finish()

func get_chunk(x,z):
	var key = str(x) + "," + str(z)
	if chunks.has(key):
		return chunks.get(key)
	else:
		return null

func _process(delta):
	update_chunks()
	clean_up_chunks()
	reset_chunks()

func update_chunks():
	var ptx = int(player_translation.x) / CHUNK_SIZE
	var ptz = int(player_translation.z) / CHUNK_SIZE
	
	for x in range(ptx - CHUNK_RENDER_DISTANCE, ptx + CHUNK_RENDER_DISTANCE):
		for z in range(ptz - CHUNK_RENDER_DISTANCE, ptz + CHUNK_RENDER_DISTANCE):
			add_chunk(x, z)
			var chunk = get_chunk(x, z)
			if chunk != null: 
				chunk.remove = false

func clean_up_chunks():
	pass

func reset_chunks():
	pass
